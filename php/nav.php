
<nav>
    <label for="menu-mob" class="menu-mob">Menu</label>
    <input type="checkbox" name="" id="menu-mob" role="button">
    <ul>

        <li class="menu-html"> <a href="/project-programmation-web/index.php">Acceuil</a></li>
        <li class="menu-cours"> <a href="">cours</a>
            <ul class="submenu">
            <li> <a href="/project-programmation-web/pages_suivantes/index_cour.php?classe=6EME&&matiere=1"> sixieme (6eme)</a></li>
                <li> <a href="/project-programmation-web/pages_suivantes/index_cour.php?classe=5EME&&matiere=1"> cinquieme (5eme)</a></li>
                <li> <a href="/project-programmation-web/pages_suivantes/index_cour.php?classe=4EME&&matiere=1"> quatrieme (4eme)</a></li>
                <li> <a href="/project-programmation-web/pages_suivantes/index_cour.php?classe=3EME&&matiere=1"> troisieme (3eme)</a></li>
                <li> <a href="/project-programmation-web/pages_suivantes/index_cour.php?classe=SECONDE&&matiere=1"> seconde (2nde)</a></li>
                <li> <a href="/project-programmation-web/pages_suivantes/index_cour.php?classe=PREMIERE&&matiere=1"> premiere (1ere)</a></li>
                <li> <a href="/project-programmation-web/pages_suivantes/index_cour.php?classe=TERMINALE&&matiere=1"> terminale (Tle)</a></li>
            </ul>
        </li>
        <li class="menu-epreuves"> <a href="">epreuves</a>
            <ul class="submenu">
            <li> <a href="/project-programmation-web/pages_suivantes/index_epreuve.php?classe=6EME&&matiere=1"> sixieme (6eme)</a></li>
                <li> <a href="/project-programmation-web/pages_suivantes/index_epreuve.php?classe=5EME&&matiere=1"> cinquieme (5eme)</a></li>
                <li> <a href="/project-programmation-web/pages_suivantes/index_epreuve.php?classe=4EME&&matiere=1"> quatrieme (4eme)</a></li>
                <li> <a href="/project-programmation-web/pages_suivantes/index_epreuve.php?classe=3EME&&matiere=1"> troisieme (3eme)</a></li>
                <li> <a href="/project-programmation-web/pages_suivantes/index_epreuve.php?classe=SECONDE&&matiere=1"> seconde (2nde)</a></li>
                <li> <a href="/project-programmation-web/pages_suivantes/index_epreuve.php?classe=PREMIERE&&matiere=1"> premiere (1ere)</a></li>
                <li> <a href="/project-programmation-web/pages_suivantes/index_epreuve.php?classe=TERMINALE&&matiere=1"> terminale (Tle)</a></li>
            </ul>
        </li>
        <li class="menu-propos"> <a href="/project-programmation-web/#foot">a propos</a></li>
    </ul>
    <a href="/project-programmation-web/pages_suivantes/inscription.php" class="I_btn">inscription</a>
</nav>
    
