<?php
      session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style/page_dacceuil.css">
    <link rel="stylesheet" href="style/nav1.css">
</head>
<body>
   
    <?php 
        include("php/entete.php");
        include("php/nav.php");
    ?>
        <div class="img">

            
            <div class="dim" id="qqq">
                <form action="traitement_connexion.php" method="post">
                        <span>CONNEXION</span> 
                    <br><center><p style='color:red'><?php  if(isset($_SESSION['erreur'])) echo $_SESSION['erreur']; ?></p></center> 
                    <p><input type="text" placeholder=" EMAIL OU TELEPHONE" class="input" name="username"></p>
                    <p><input type="password" placeholder=" MOT DE PASSE " class="input" name="password"></p>
                   
                    <p><input type="submit" value="CONNEXION" class="button"></p>
                    <p><input type="reset" value="EFFACER" class="button"></p>
                </form>
                <center>Vous n'avez pas de compte? <a href="pages_suivantes/inscription.php" class="lien">incrivez-vous</a></center>
                
        </div>

    <div class="savoir"><h1>A SAVOIR</h1> </div>
    <section class="section1">
       
        <article class="art1">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum obcaecati est illo veritatis quas, facere dolorem odit voluptates expedita ipsa blanditiis molestias aliquam quidem quibusdam ut neque incidunt quo! Tempore.
        </article>
        <article class="art1">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsum minima facilis odio dolorum. Facilis pariatur a dolores obcaecati iste neque numquam suscipit natus! Beatae reprehenderit, veniam soluta asperiores deserunt sit.
        </article>
        <article class="art1" >
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugiat quia adipisci reiciendis totam dolorum exercitationem maxime inventore voluptate, quisquam omnis at quod dicta dolor quis! Quis nulla nihil voluptatibus reprehenderit.
        </article>
    </section>
   
   
    <?php 
        include("php/footer.php");
    ?>
    <script src="js/regexConnexion.js"></script>
    <script src="js/scrollFix.js"></script>
</body>
</html>
<?php
    session_destroy();
?>