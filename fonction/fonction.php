<?php
    require 'database.php';
    /**
     * cette page doit contenir l'ensemble des fonctions php qui entre dans la conception du site
     * @author Dimitri
     */

    function insertion(){
        global $db;
        

    $rq=$db->prepare("SELECT * from utilisateur where email=:email or telephone=:tel");
    $rq->execute(array(
        'email'=>$_POST['email'],
        'tel'=>$_POST['telephone']
    ));

    if ($_POST['pass']==$_POST['pss']) {
        $donne=($rq->fetch());
        if(isset($donne['email'])){
            $hach=sha1($_POST['pass']);
            $req=$db->prepare("INSERT INTO utilisateur(nom,prenom,email,mot_de_passe,classe,telephone,statut,date_creation) values (:nom,:prenom,:email,:mot_de_passe,:classe,:telephone,:statut,CURDATE())");
            $req->execute(array(
            'nom'=>$_POST['nom'],
            'prenom'=>$_POST['prenom'],
            'email'=>$_POST['email'],
            'mot_de_passe'=>$hach,
            'classe'=>1,
            'telephone'=>$_POST['telephone'],
            'statut'=>'client'
            ));
            header("Location:/project-programmation-web/index.php");
        }
        else{
            session_start();
            $_SESSION['error']="identifiant existant";
            header("Location:/project-programmation-web/pages_suivantes/inscription.php");
        }
    
    }else{
        session_start();
        $_SESSION['error1']="erreur de comfirmation";
            header("Location:/project-programmation-web/pages_suivantes/inscription.php");
    }

    
        
    }



    //INSERTION DES EPREUVES
    function insertion_admin()
    {

        global $db;
        $rq=$db->prepare("INSERT INTO epreuve(nom,classe,matiere,date_depot,url_epreuve)values(:nom,:classe,:matiere,CURDATE(),:url_epreuve) ");
        $rq->execute(array(
                           "nom"=>$file_name,
                           "classe"=>$_POST['epreuve'],
                           "matiere"=>$_POST['matiere'],
                           "url_epreuve"=>$file_dest
                    ));
        
    }
/**
 * cette fonction retourne un objet contenant tous les matieres contenu dans la table matiere
 */
    function recup_matiere(){
        global $db;
        $req=$db->prepare("SELECT * FROM matiere ");
        $req->execute(array());
        return $req;
    }

/**
 * cette fonction retourne un objet contenant tous les classes contenu dans la table classe
 */
    function recup_classe(){
        global $db;
        $req=$db->prepare("SELECT * FROM classe ");
        $req->execute(array());
        return $req;
    }

    /**
 * cette fonction ajoute une matiere
 */
function ajout_matiere($nom_post){
    global $db;
    $req=$db->prepare("INSERT INTO matiere(nom)values(:nom) ");
    $req->execute(array("nom"=>$nom_post));
}

//SELECTION DE L'ADMIN CONNECTE
function select_admin($idd){
    global $db;
        $req=$db->prepare("SELECT * FROM utilisateur where id=:id ");
        $req->execute(array('id'=>$idd));
        return $req;
}

//modifier les parametres de ladmin
function update_admin($nom , $prenom, $email, $pass,$tel, $idd){
    global $db;
        $req=$db->prepare("UPDATE FROM utilisateur SET nom=:nom prenom=:prenom email=:email mot_de_passe=:pass telephone=:tel where id=:id ");
        $req->execute(array('id'=>$idd,
                            'nom'=>$nom,
                            'prenom'=>$prenom,
                            'email'=>$email,
                            'pass'=>$pass,
                            'tel'=>$tel 
                        ));
        return $req;
}

//afficher les membres
function affiche_membre($statut)
{
    Global $db;
    $req=$db->prepare("SELECT * FROM utilisateur where statut=:statut");
    $req->execute(array('statut'=>$statut));
    return $req;
}

//tout les membres
function affiche_tout_membre(){
    Global $db;
    $req=$db->prepare("SELECT * FROM utilisateur");
    $req->execute(array());
    return $req;
}

//supression utilisateur

function supression($idd){
    Global $db;
    $req=$db->prepare("DELETE  FROM utilisateur  where id=:id");
    $req->execute(array('id'=>$idd));
    
}

//supression matiere

function supression_matiere($idd){
    Global $db;
    $req=$db->prepare("DELETE  FROM matiere  where id=:id");
    $req->execute(array('id'=>$idd));
    
}

//affiche matiere
function affiche_matier(){
    Global $db;
    $req=$db->prepare("SELECT *  FROM matiere ");
    $req->execute(array());
    return $req;
}

//affiche cour
function affiche_cour(){
    Global $db;
    $req=$db->prepare("SELECT *  FROM cour ");
    $req->execute(array());
    return $req;
}

//affiche epreuve
function affiche_epreuve(){
    Global $db;
    $req=$db->prepare("SELECT *  FROM epreuve ");
    $req->execute(array());
    return $req;
}

//supression cour

function supression_cour($idd){
    Global $db;
    $req=$db->prepare("DELETE  FROM cour  where id=:id");
    $req->execute(array('id'=>$idd));
    
}

//supression epreuve

function supression_epreuve($idd){
    Global $db;
    $req=$db->prepare("DELETE  FROM epreuve  where id=:id");
    $req->execute(array('id'=>$idd));
    
}
?>