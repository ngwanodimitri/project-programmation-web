-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : mer. 16 juin 2021 à 09:47
-- Version du serveur :  10.4.11-MariaDB
-- Version de PHP : 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `site`
--

-- --------------------------------------------------------

--
-- Structure de la table `classe`
--

CREATE TABLE `classe` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `date_creation` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `classe`
--

INSERT INTO `classe` (`id`, `nom`, `date_creation`) VALUES
(1, '3EME', '2021-05-18 14:28:00'),
(2, '6EME', '2021-06-01 15:16:38'),
(3, '5EME', '2021-06-01 15:16:38'),
(6, '4EME', '2021-06-01 15:17:12'),
(7, 'SECONDE', '2021-06-01 16:45:44'),
(8, 'PREMIERE', '2021-06-01 16:45:44'),
(9, 'TERMINALE', '2021-06-01 16:45:58');

-- --------------------------------------------------------

--
-- Structure de la table `commentaire`
--

CREATE TABLE `commentaire` (
  `id` int(11) NOT NULL,
  `commentateur` int(11) NOT NULL,
  `contenu` text NOT NULL,
  `date_envoie` datetime NOT NULL DEFAULT current_timestamp(),
  `id_matiere` int(11) DEFAULT NULL,
  `id_classe` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `commentaire`
--

INSERT INTO `commentaire` (`id`, `commentateur`, `contenu`, `date_envoie`, `id_matiere`, `id_classe`) VALUES
(1, 232, 'bonjour bonjirr', '2021-06-09 00:00:00', NULL, NULL),
(2, 232, 'bonjour bonjirr', '2021-06-09 00:00:00', NULL, NULL),
(3, 232, 'bonjour bonjirr', '2021-06-09 00:00:00', NULL, NULL),
(4, 232, 'bonjour bonjirr', '2021-06-09 00:00:00', NULL, NULL),
(5, 232, 'bonjour bonjirr', '2021-06-09 00:00:00', NULL, NULL),
(6, 232, 'bonjour bonjirr', '2021-06-09 00:00:00', NULL, NULL),
(7, 232, 'bonjour bonjirr', '2021-06-09 00:00:00', NULL, NULL),
(8, 232, 'bonjour bonjirr', '2021-06-09 00:00:00', NULL, NULL),
(9, 232, 'bonjour bonjirr', '2021-06-09 00:00:00', NULL, NULL),
(10, 232, 'bonjour bonjirr', '2021-06-09 00:00:00', NULL, NULL),
(11, 232, 'bonjour bonjirr', '2021-06-09 00:00:00', NULL, NULL),
(12, 232, 'bonjour bonjirr', '2021-06-09 00:00:00', NULL, NULL),
(13, 232, 'bonjour bonjirr', '2021-06-09 00:00:00', NULL, NULL),
(14, 232, 'bonjour bonjirr', '2021-06-09 00:00:00', NULL, NULL),
(15, 232, 'bonjour bonjirr', '2021-06-09 00:00:00', NULL, NULL),
(16, 232, 'bonjour bonjirr', '2021-06-09 00:00:00', NULL, NULL),
(17, 232, 'bonjour bonjirr', '2021-06-09 00:00:00', NULL, NULL),
(18, 232, 'bonjour bonjirr', '2021-06-09 00:00:00', NULL, NULL),
(19, 232, 'bonjour bonjirr', '2021-06-09 00:00:00', NULL, NULL),
(20, 232, 'bonjour bonjirr', '2021-06-09 00:00:00', NULL, NULL),
(21, 232, 'bonjour bonjirr', '2021-06-09 00:00:00', NULL, NULL),
(22, 232, 'bonjour bonjirr', '2021-06-09 00:00:00', NULL, NULL),
(23, 232, 'bonjour bonjirr', '2021-06-09 00:00:00', NULL, NULL),
(25, 232, 'bonjour bonjirr', '2021-06-09 00:00:00', NULL, NULL),
(27, 232, 'pas mal la ba', '2021-06-09 00:00:00', NULL, NULL),
(28, 232, 'si non tu vie \r\net qio de neuf\r\n', '2021-06-09 00:00:00', NULL, NULL),
(29, 232, 'dfdfgdeofjeogkrjgrogjogjogjoejfoefjoefjoefjoejfoejfeofje\r\n', '2021-06-10 00:00:00', NULL, NULL),
(30, 232, 'xbdxbdxbdbdb', '2021-06-10 00:00:00', NULL, NULL),
(31, 257, 'fejfiefjeifji', '2021-06-10 00:00:00', NULL, NULL),
(32, 257, 'fejfiefjeifji', '2021-06-10 00:00:00', NULL, NULL),
(33, 257, 'vdvd', '2021-06-10 00:00:00', NULL, NULL),
(34, 257, 'k', '2021-06-10 00:00:00', NULL, NULL),
(35, 258, 'cvdvdvdvdvdv', '2021-06-10 00:00:00', NULL, NULL),
(36, 258, 'c\'est une bonne epreuve', '2021-06-10 00:00:00', NULL, NULL),
(37, 258, '55', '2021-06-10 00:00:00', NULL, NULL),
(38, 232, '525', '2021-06-10 00:00:00', NULL, NULL),
(39, 232, '565', '2021-06-10 00:00:00', NULL, NULL),
(40, 232, '5', '2021-06-10 00:00:00', NULL, NULL),
(41, 232, 'gkofgkofgk', '2021-06-10 00:00:00', NULL, NULL),
(42, 232, 'dvgldfld,', '2021-06-10 00:00:00', NULL, NULL),
(43, 232, 'dggd', '2021-06-10 21:50:40', NULL, NULL),
(44, 232, 't4t4t4t', '2021-06-10 21:51:06', NULL, NULL),
(45, 232, 'sfsfsf', '2021-06-10 21:51:45', NULL, NULL),
(46, 232, 'vdv', '2021-06-10 22:11:59', NULL, NULL),
(47, 232, 'jlhj', '2021-06-10 22:14:18', NULL, NULL),
(48, 232, 'fhgjhkjlk\r\n', '2021-06-10 22:14:38', NULL, NULL),
(49, 232, 'fgjhkjlk;l', '2021-06-10 22:14:52', NULL, NULL),
(50, 226, 'fbfb', '2021-06-10 22:44:26', NULL, NULL),
(51, 226, 'dfbdfbdfb', '2021-06-10 22:44:32', NULL, NULL),
(52, 226, 'c vcdvc', '2021-06-10 22:46:42', NULL, NULL),
(53, 226, '  ccc', '2021-06-10 23:15:36', NULL, NULL),
(54, 226, 'ccccccccccccccccccccccccccccccccccccccccccccccccccc', '2021-06-10 23:15:43', NULL, NULL),
(55, 226, 'jkl;', '2021-06-10 23:35:36', NULL, NULL),
(56, 226, 'kfkf', '2021-06-11 00:19:06', NULL, NULL),
(57, 226, 'kfkf', '2021-06-11 00:19:06', NULL, NULL),
(58, 226, 'kfkf', '2021-06-11 00:19:06', NULL, NULL),
(59, 226, '', '2021-06-11 00:26:29', NULL, NULL),
(60, 226, '', '2021-06-11 00:26:32', NULL, NULL),
(61, 226, 'g', '2021-06-11 00:26:36', NULL, NULL),
(62, 226, 'gdgd', '2021-06-11 06:26:59', NULL, NULL),
(63, 232, 'ssssss', '2021-06-11 06:31:48', NULL, NULL),
(64, 232, 'fbdfgbvdv', '2021-06-11 06:32:34', NULL, NULL),
(65, 232, 'gygygygy', '2021-06-11 06:45:30', NULL, NULL),
(66, 226, 'gàeigoeàgieài', '2021-06-11 08:15:15', NULL, NULL),
(67, 226, 'lgpeglepg', '2021-06-11 08:15:41', NULL, NULL),
(68, 226, '\r\nRTDR', '2021-06-11 08:21:37', NULL, NULL),
(69, 232, 'hfhfh bonjour\r\n', '2021-06-14 07:28:30', NULL, NULL),
(70, 232, 'x b', '2021-06-14 07:58:06', NULL, NULL),
(71, 232, '', '2021-06-14 07:58:07', NULL, NULL),
(72, 232, 'vsssv', '2021-06-14 07:58:13', NULL, NULL),
(73, 232, 'ccccccv', '2021-06-14 07:58:26', NULL, NULL),
(74, 232, 'vvc', '2021-06-14 07:58:45', NULL, NULL),
(75, 232, 'cc c\'est bien ', '2021-06-14 07:59:18', NULL, NULL),
(76, 232, 'fortohgkr', '2021-06-14 09:26:51', NULL, NULL),
(77, 232, 'rrgrgrgr', '2021-06-14 09:40:47', NULL, NULL),
(78, 232, 'pourquoi\r\n', '2021-06-14 17:02:17', NULL, NULL),
(79, 232, '', '2021-06-14 17:02:19', NULL, NULL),
(80, 232, ' x x', '2021-06-14 17:26:35', NULL, NULL),
(81, 232, 'dbdbd', '2021-06-14 18:27:53', NULL, NULL),
(82, 232, 'svvsv', '2021-06-14 18:42:26', 1, NULL),
(83, 232, 'xvvv', '2021-06-14 18:42:54', 1, NULL),
(84, 232, 'dbsvb', '2021-06-14 18:44:23', 1, NULL),
(85, 232, 'svsvsx', '2021-06-14 18:44:28', 1, NULL),
(86, 232, 'xv xv/\'x/', '2021-06-14 18:44:34', 1, NULL),
(87, 232, 'egeg', '2021-06-14 18:47:47', 1, NULL),
(88, 232, 'dbdc ', '2021-06-14 18:49:41', 1, 2),
(89, 232, 'xvxv', '2021-06-14 18:49:50', 2, 2),
(90, 232, 'dbd]b\'', '2021-06-14 18:50:02', 3, 2),
(91, 232, 'vsbvs', '2021-06-14 18:50:29', 3, 7),
(92, 232, 'jtjtjthdgbd', '2021-06-14 18:50:55', 3, 7),
(93, 232, 'jtjtjthdgbd', '2021-06-14 18:50:55', 3, 7),
(94, 232, 'zv zv ', '2021-06-14 18:51:01', 3, 7),
(95, 232, 'rheheh', '2021-06-14 18:55:46', 2, 3),
(96, 232, 'bdbdb', '2021-06-14 20:41:58', 1, 2),
(97, 232, '', '2021-06-14 20:41:59', 1, 2),
(98, 232, 'wtgewtewt', '2021-06-14 20:42:25', 1, 2),
(99, 232, 'fsf', '2021-06-14 20:44:38', 1, 2),
(100, 232, 'gdgd', '2021-06-14 20:44:45', 2, 2),
(101, 232, 'dgdgdgv', '2021-06-14 20:44:51', 4, 2),
(102, 232, 'dbdvbd', '2021-06-14 20:44:59', 5, 2),
(103, 232, 'bk,bk', '2021-06-14 20:45:44', 1, 3),
(104, 232, 'ged', '2021-06-14 20:48:28', 1, 3),
(105, 232, 'hum', '2021-06-14 20:48:36', 2, 3),
(106, 232, 'dvodik', '2021-06-14 20:48:44', 3, 3),
(107, 232, 'procvvfs', '2021-06-14 20:48:53', 5, 3),
(108, 232, 'jjm', '2021-06-14 20:49:20', 1, 7),
(109, 232, 'jhujhi', '2021-06-14 20:49:31', 2, 7),
(110, 232, 'nkn', '2021-06-14 20:49:52', 3, 7),
(111, 232, 'niknknnlkhik', '2021-06-14 20:50:15', 5, 7),
(112, 232, 'hkjhkj', '2021-06-14 20:50:27', 4, 7),
(113, 232, 'ddvdvdv', '2021-06-14 21:26:25', 1, 9),
(114, 232, 'tikitijti', '2021-06-15 09:54:30', 1, 3),
(115, 232, 'tlktok', '2021-06-15 09:54:51', 4, 3),
(116, 232, 'c c ', '2021-06-15 09:55:10', 4, 9),
(117, 232, 'fgfb', '2021-06-15 12:36:38', 1, 2),
(118, 232, '', '2021-06-15 13:02:33', 1, 2),
(119, 232, '', '2021-06-15 13:02:35', 1, 2),
(120, 232, '', '2021-06-15 13:03:24', 1, 2),
(121, 232, '', '2021-06-15 13:03:25', 1, 2),
(122, 232, '', '2021-06-15 13:03:27', 1, 2),
(123, 232, 'dvsd', '2021-06-15 13:05:34', 1, 2),
(124, 232, 'dvdv', '2021-06-15 13:05:39', 1, 2),
(125, 232, 'sfs', '2021-06-15 13:05:45', 3, 2),
(126, 232, 'pokokokoko', '2021-06-15 13:06:38', 1, 2),
(127, 232, 'x x', '2021-06-15 13:10:43', 1, 2),
(128, 232, 'b', '2021-06-15 13:10:54', 1, 2),
(129, 232, '   ', '2021-06-15 13:11:51', 1, 2),
(130, 232, 'ok info', '2021-06-15 13:13:15', 3, 2),
(131, 259, 'fdvdv', '2021-06-15 13:27:24', 1, 2),
(132, 259, 'dfd', '2021-06-15 13:49:43', 1, 2),
(133, 224, 'bonjour\r\n', '2021-06-15 19:03:09', 1, 2),
(134, 224, 'bonjour', '2021-06-15 19:03:23', 4, 2);

-- --------------------------------------------------------

--
-- Structure de la table `cours`
--

CREATE TABLE `cours` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `matiere` int(11) NOT NULL,
  `classe` int(11) NOT NULL,
  `date_depot` datetime NOT NULL DEFAULT current_timestamp(),
  `url_cour` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `cours`
--

INSERT INTO `cours` (`id`, `nom`, `matiere`, `classe`, `date_depot`, `url_cour`) VALUES
(1, '309961-dynamisez-vos-sites-web-avec-javascript.pdf', 1, 1, '2021-06-16 09:38:00', 'epreuve_file/309961-dynamisez-vos-sites-web-avec-javascript.pdf');

-- --------------------------------------------------------

--
-- Structure de la table `epreuve`
--

CREATE TABLE `epreuve` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `classe` int(11) NOT NULL,
  `matiere` int(11) NOT NULL,
  `date_depot` datetime NOT NULL DEFAULT current_timestamp(),
  `url_epreuve` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `epreuve`
--

INSERT INTO `epreuve` (`id`, `nom`, `classe`, `matiere`, `date_depot`, `url_epreuve`) VALUES
(5, '4-html5_liste_balises (1).pdf', 1, 1, '2021-06-15 21:15:37', 'epreuve_file/4-html5_liste_balises (1).pdf'),
(6, '309961-dynamisez-vos-sites-web-avec-javascript.pdf', 1, 1, '2021-06-16 09:37:52', 'epreuve_file/309961-dynamisez-vos-sites-web-avec-javascript.pdf');

-- --------------------------------------------------------

--
-- Structure de la table `matiere`
--

CREATE TABLE `matiere` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `date_creation` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `matiere`
--

INSERT INTO `matiere` (`id`, `nom`, `date_creation`) VALUES
(1, 'histoire', '2021-06-01 15:19:22'),
(2, 'geographie', '2021-06-01 15:19:22'),
(3, 'informatique', '2021-06-01 15:19:53'),
(4, 'mathematique', '2021-06-01 15:19:53'),
(5, 'francais', '2021-06-01 15:34:32'),
(6, 'philosophie', '2021-06-15 19:41:54'),
(7, 'svt', '2021-06-15 19:42:06'),
(8, 'info', '2021-06-15 19:47:39'),
(9, 'espagnol', '2021-06-16 08:28:20');

-- --------------------------------------------------------

--
-- Structure de la table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `cont_message` text NOT NULL,
  `destinataire` int(11) NOT NULL,
  `destinateur` int(11) NOT NULL,
  `date_envoie` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mot_de_passe` varchar(255) NOT NULL,
  `classe` int(11) NOT NULL,
  `telephone` int(12) NOT NULL,
  `statut` varchar(255) NOT NULL,
  `date_creation` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `nom`, `prenom`, `email`, `mot_de_passe`, `classe`, `telephone`, `statut`, `date_creation`) VALUES
(224, 'tueam', 'dimitri', 'ngwanodimitri@gmail.com', '5edbd1ca257c9946ee7766c7bfb95aed61016376', 1, 2147483647, 'client', '2021-05-31 00:00:00'),
(225, 'admin', 'admin', 'admin@gmail.com', 'd033e22ae348aeb5660fc2140aec35850c4da997', 1, 2147483647, 'admin', '2021-05-31 00:00:00'),
(226, 'dimig', 'francko', 'franckngakou@gmail.com', '02bc528f101952568c13ef4357268e34e0c6ae07', 1, 2147483647, 'client', '2021-06-01 00:00:00'),
(227, 'NGAKOU cfvjdvjdv', 'wisss-6', 'franck@gmail.com', '5d44a7b04e40ecc899fdd2269e8603d64a9478f2', 1, 652348523, 'client', '2021-06-03 00:00:00'),
(228, 'nnfvvbf6856', 'ndbgdfg', 'n@gmail.com', 'e57a14bb5a3ccdc260d173d989d187d86d4aabfa', 1, 2147483647, 'client', '2021-06-04 00:00:00'),
(230, 'bonjourvcg', 'franck4', 'nga@gmail.com4', '02bc528f101952568c13ef4357268e34e0c6ae07', 1, 2147483647, 'client', '2021-06-05 00:00:00'),
(231, 'ngakou', 'franck', 'ga@gmail.com', '02bc528f101952568c13ef4357268e34e0c6ae07', 1, 690057470, 'client', '2021-06-05 00:00:00'),
(232, 'francky', 'wilfried', 'n4ga@gmail.com', '02bc528f101952568c13ef4357268e34e0c6ae07', 1, 2147483647, 'client', '2021-06-05 00:00:00'),
(233, 'ngakou', 'francky', 'a@gmail.com', '02bc528f101952568c13ef4357268e34e0c6ae07', 1, 2147483647, 'client', '2021-06-05 00:00:00'),
(240, 'ngakou', 'francky', 'franckY@gmail.com', '02bc528f101952568c13ef4357268e34e0c6ae07', 1, 2147483647, 'client', '2021-06-05 00:00:00'),
(244, 'bonjour', 'franck', 'fgfranck@gmail.com', '02bc528f101952568c13ef4357268e34e0c6ae07', 1, 2147483647, 'client', '2021-06-05 00:00:00'),
(246, 'rtrtr', 'dfsdf     ', 'ka@gmail.com', '02bc528f101952568c13ef4357268e34e0c6ae07', 1, 2147483647, 'client', '2021-06-05 00:00:00'),
(254, 'ngakou', 'francky', 'tu@gmail.com', '02bc528f101952568c13ef4357268e34e0c6ae07', 1, 2147483647, 'client', '2021-06-06 00:00:00'),
(255, 'ngakou', 'francky', 'Za@gmail.com', '02bc528f101952568c13ef4357268e34e0c6ae07', 1, 2147483647, 'client', '2021-06-06 00:00:00'),
(256, 'eeder', 'fhjt', 'ra@gmail.com', '02bc528f101952568c13ef4357268e34e0c6ae07', 1, 2147483647, 'client', '2021-06-09 00:00:00'),
(257, 'bonjour', 'franck', 'ja@gmail.com', 'd7e4e9abedd0949b8bcff30c7abbdad97b182be8', 1, 2147483647, 'client', '2021-06-10 00:00:00'),
(258, 'ngakou', 'francky', 'oa@gmail.com', '55c82571cfe8abaae465997ef548957e7589d913', 1, 2147483647, 'client', '2021-06-10 00:00:00'),
(259, 'ngakou', 'franck', '9a@gmail.com', '55c82571cfe8abaae465997ef548957e7589d913', 1, 2147483647, 'client', '2021-06-15 00:00:00'),
(261, 'thierry', 'Tueam', 'g@gmaio.com', '273a0c7bd3c679ba9a6f5d99078e36e85d02b952', 1, 2147483647, 'client', '2021-06-16 00:00:00');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `classe`
--
ALTER TABLE `classe`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `commentaire`
--
ALTER TABLE `commentaire`
  ADD PRIMARY KEY (`id`),
  ADD KEY `commentateur` (`commentateur`),
  ADD KEY `id_matiere` (`id_matiere`),
  ADD KEY `id_classe` (`id_classe`);

--
-- Index pour la table `cours`
--
ALTER TABLE `cours`
  ADD PRIMARY KEY (`id`),
  ADD KEY `matiere` (`matiere`),
  ADD KEY `classe` (`classe`);

--
-- Index pour la table `epreuve`
--
ALTER TABLE `epreuve`
  ADD PRIMARY KEY (`id`),
  ADD KEY `classe` (`classe`),
  ADD KEY `matiere` (`matiere`);

--
-- Index pour la table `matiere`
--
ALTER TABLE `matiere`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `destinataire` (`destinataire`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `classe` (`classe`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `classe`
--
ALTER TABLE `classe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `commentaire`
--
ALTER TABLE `commentaire`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=135;

--
-- AUTO_INCREMENT pour la table `cours`
--
ALTER TABLE `cours`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `epreuve`
--
ALTER TABLE `epreuve`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `matiere`
--
ALTER TABLE `matiere`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=262;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `commentaire`
--
ALTER TABLE `commentaire`
  ADD CONSTRAINT `commentaire_ibfk_1` FOREIGN KEY (`commentateur`) REFERENCES `utilisateur` (`id`),
  ADD CONSTRAINT `commentaire_ibfk_2` FOREIGN KEY (`id_matiere`) REFERENCES `matiere` (`id`),
  ADD CONSTRAINT `commentaire_ibfk_3` FOREIGN KEY (`id_classe`) REFERENCES `classe` (`id`);

--
-- Contraintes pour la table `cours`
--
ALTER TABLE `cours`
  ADD CONSTRAINT `cours_ibfk_1` FOREIGN KEY (`matiere`) REFERENCES `matiere` (`id`),
  ADD CONSTRAINT `cours_ibfk_2` FOREIGN KEY (`classe`) REFERENCES `classe` (`id`);

--
-- Contraintes pour la table `epreuve`
--
ALTER TABLE `epreuve`
  ADD CONSTRAINT `epreuve_ibfk_1` FOREIGN KEY (`classe`) REFERENCES `classe` (`id`),
  ADD CONSTRAINT `epreuve_ibfk_2` FOREIGN KEY (`matiere`) REFERENCES `matiere` (`id`);

--
-- Contraintes pour la table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `message_ibfk_1` FOREIGN KEY (`destinataire`) REFERENCES `utilisateur` (`id`);

--
-- Contraintes pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD CONSTRAINT `utilisateur_ibfk_1` FOREIGN KEY (`classe`) REFERENCES `classe` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
