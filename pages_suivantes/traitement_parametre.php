<?php
 session_start();
 include_once("../fonction/database.php");
 if (isset($_POST['nom'])) {
    $req=$db->prepare("UPDATE utilisateur SET nom= :nom WHERE id=:id");
    $req->execute(array( 
        'id'=>$_SESSION['id'],
        'nom'=>$_POST['nom']
    ));
    header("Location:parametre.php");
}
if (isset($_POST['prenom'])) {
    $req=$db->prepare("UPDATE utilisateur SET prenom= :prenom WHERE id=:id");
    $req->execute(array( 
        'id'=>$_SESSION['id'],
        'prenom'=>$_POST['prenom']
    ));
    header("Location:parametre.php");
}
if (isset($_POST['email'])) {
    $req=$db->prepare("UPDATE utilisateur SET email= :email WHERE id=:id");
    $req->execute(array( 
        'id'=>$_SESSION['id'],
        'email'=>$_POST['email']
    ));
    header("Location:parametre.php");
}
if (isset($_POST['telephone'])) {
    $req=$db->prepare("UPDATE utilisateur SET telephone= :telephone WHERE id=:id");
    $req->execute(array( 
        'id'=>$_SESSION['id'],
        'telephone'=>$_POST['telephone']
    ));
    header("Location:parametre.php");
}