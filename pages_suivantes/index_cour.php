<?php
    
    session_start();
    if(isset($_GET['classes'])){
        $_SESSION['classes']=$_GET['classes'];
    }
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../style/page_dacceuil.css">
    <link rel="stylesheet" href="../style/cour.css">
	<link rel="stylesheet" href="../style/nav1.css">
    <link rel="stylesheet" href="../style/nav33.css">
    <link rel="stylesheet" href="../style/commentaire.css">
</head>
<body>
	<?php
        
        if(isset($_SESSION['id'])){
            include("../php/nav1.php");
        }else{
            include("../php/nav.php");
        }
    ?>
    <section class="c_section1">
    	
    		<article class="c_art1">
    			<header class="c_head1"> <center> <?php if (isset($_SESSION['classes'])){echo $_SESSION['classes'];}else{echo 'CLASSE';} ?></center> </header>
    			
				<?php include 'layout_cour/matiere.php'; ?>

    		</article>

    		<article class="c_art2">
				<?php include 'layout_cour/recherche.php'; ?>
    			<?php include 'layout_cour/ensembleCour.php'; ?>
    		</article>

			<?php include 'layout_cour/commentaire.php'; ?>

    </section>
</body>
</html>