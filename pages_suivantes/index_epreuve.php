
<?php
    
    session_start();
    if(isset($_GET['classe'])){
        $_SESSION['classe']=$_GET['classe'];
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="/your-path-to-fontawesome/css/fontawesome.css" rel="stylesheet">
  <link href="/your-path-to-fontawesome/css/brands.css" rel="stylesheet">
  <link href="/your-path-to-fontawesome/css/solid.css" rel="stylesheet">
    <link rel="stylesheet" href="../style/epreuve.css">
    <link rel="stylesheet" href="../style/nav1.css">
    <link rel="stylesheet" href="../style/nav.css">
    <link rel="stylesheet" href="../style/commentaire.css">

    <title>Document</title>
</head>
<body>
    <?php
        
        if(isset($_SESSION['id'])){
            include("../php/nav1.php");
        }else{
            include("../php/nav.php");
        }
    ?>

    <div class="global">
            <aside class="E_assideG">
                <div class="E_classe">
                    <center><h1 class="hh"><?php if (isset($_SESSION['classe'])){echo $_SESSION['classe'];}else{echo 'CLASSE';} ?></h1></center>
                    <hr>
                </div>
                <?php include 'layout/matiere.php'; ?>
            
            </aside>

            <?php include 'layout/recherche.php'; ?>
                    <div class="E_ecriture main">
                        <?php include 'layout/ensembleEpreuve.php'; ?>  
                    </div>
            </section>
                <?php include 'layout/commentaire.php'; ?>
    </div>
       
       
    
    <script src="../js/recherche.js"></script>
</body>
</html>