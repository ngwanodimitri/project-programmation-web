<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../style/page_dacceuil.css">
    <link rel="stylesheet" href="../style/inscription.css">
   
</head>
<body>
<header>
        <h1>ETUDE LIBRE</h1>
        Etudes pour tous    
</header>
   <div>
       <div><h4>INSCRIPTION</h4></div> 
       
    <form action="/project-programmation-web/traitement_inscription.php" method="post" onsubmit="return soumis();">

    <p style="color:red ;text-align:center">  <?php  if(isset($_SESSION['error']))  echo $_SESSION['error'];  ?> </p>
    
    <p><label for="nom">NOM</label><input  type="text" name="nom" id="nom" size="30"   placeholder="SAISIR VOTRE NOM"></p>
    <p><label for="prenom">PRENOM</label><input type="text" name="prenom" id="prenom" size="30"   placeholder="SAISIR VOTRE PRENOM"></p>
    <p><label for="telephone">TELEPHONE</label><input type="text" name="telephone" id="telephone" size="30"  placeholder="SAISIR VOTRE NUMERO DE TELEPHONE +237XXXXXXXXX"></p>
    <p><label for="email">EMAIL</label><input type="email" name="email" id="email" size="30"  placeholder="SAISIR VOTRE ADRESSE EMAIL"></p>
    <p><label for="pass">MOT DE PASSE</label><input type="password" name="pass" id="pass" size="30" placeholder="SAISIR VOTRE MOT DE PASSE"></p>
   
    <p style="color:red ;text-align:center">  <?php  if(isset($_SESSION['error1'])) echo $_SESSION['error1'];  ?> </p>

    <p><label for="pss">CONFIRMER</label><input type="password" name="pss" id="pss" size="30" placeholder="CONFIRMER VOTRE MOT DE PASSE"></p>
  <p><input type="submit" name=""  id="btn" value="login" onclick=""size="30"></p>  
  <center>Vous avez deja une compte? <a href="../index.php" class="lien">conectez-vous</a></center>
    </form>
    </div>

    <?php 
        include("../php/footer.php");
    ?>
     <script src="../js/inscription.js"></script>
     <script src="../js/scrollFix.js"></script>
</body>
</html>
<?php
    session_destroy();
?>