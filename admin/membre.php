<?php
    session_start();
    require '../fonction/fonction.php';
    $rq=affiche_membre('client');
    $req=affiche_membre('admin');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="admin.css">
</head>
<body>
    <?php include("haut.php"); ?>
    <article class="droite">
            <div class="membre_admin">

                    <h1>membres administrateurs</h1>
                    
                    <table class="table" >
                        <tr class="thead"><th>numero</th><th>nom</th><th>prenom</th><th>email</th><th>telephone</th></tr>
                        <?php
                            $cpt=1;
                                while ($elem=$req->fetch()) {
                        ?>
                                    <tr height="40px" ><td width="10%"><?php echo $cpt;  ?></td><td width="28%"><?php echo $elem['nom'];  ?></td><td width="28%"><?php echo $elem['prenom'];  ?></td><td width="30%"><?php echo $elem['email'];  ?></td><td width="25%"><?php echo $elem['telephone'];  ?></td></tr>
                        <?php
                           $cpt=$cpt+1;     }
                        ?>
                    </table>
            </div>

            <div class="membre_utilisateur">

                    <h1>membres utilisateur</h1>
                    <table class="table" >
                        <tr class="thead"><th>numero</th><th>nom</th><th>prenom</th><th>email</th><th>telephone</th></tr>
                        <?php
                            $cpt=1;
                                while ($elem=$rq->fetch()) {
                        ?>
                                    <tr height="40px" ><td width="10%"><?php echo $cpt;  ?></td><td width="28%"><?php echo $elem['nom'];  ?></td><td width="28%"><?php echo $elem['prenom'];  ?></td><td width="30%"><?php echo $elem['email'];  ?></td><td width="25%"><?php echo $elem['telephone'];  ?></td></tr>
                        <?php
                           $cpt=$cpt+1;     }
                        ?>
                    </table>
            </div>
    </article>
    <?php include("bas.php"); ?>
</body>
</html>