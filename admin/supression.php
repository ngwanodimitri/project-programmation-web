<?php
    session_start();
    require '../fonction/fonction.php';
    $rq=affiche_tout_membre();
    $req=affiche_matier();
    $reqC=affiche_cour();
    $reqE=affiche_epreuve();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="admin.css">
</head>
<body>
    <?php 
        include("haut.php");
    ?>
     <article class="droite">

                <div class="membre_admin">

                            <h1>suprimer utilisateur</h1>
                            <table class="table" >
                                <tr class="thead"><th>numero</th><th>nom</th><th>prenom</th><th>email</th><th>telephone</th><th>statut</th><th>supression</th></tr>
                                <?php
                                    $cpt=1;
                                        while ($elem=$rq->fetch()) {
                                            
                                ?>
                                            <tr height="40px" ><td width="8%"><?php echo $cpt;  ?></td><td width="17%"><?php echo $elem['nom'];  ?></td><td width="17%"><?php echo $elem['prenom'];  ?></td><td width="20%"><?php echo $elem['email'];  ?></td><td width="25%"><?php echo $elem['telephone'];  ?></td><td  width="10%"><?php echo $elem['statut'];  ?></td><td width="15%"><form action="traitement_supression.php" method="post"> <input type="hidden" name="id" value="<?php echo $elem['id']; ?>"><input type="submit" value="supprimer"></form></td></tr>
                                <?php
                                $cpt=$cpt+1;     }
                                ?>
                            </table>
                 </div>

                 <div class="membre_admin">

                            <h1>suprimer matiere</h1>
                            <table class="table" >
                                <tr class="thead"><th>numero</th><th>nom</th><th>supression</th></tr>
                                <?php
                                    $cpt=1;
                                        while ($elem=$req->fetch()) {
                                            
                                ?>
                                            <tr height="40px" ><td width="8%"><?php echo $cpt;  ?></td><td width="17%"><?php echo $elem['nom'];  ?></td><td width="15%"><form action="traitement_supresion_matiere.php" method="post"> <input type="hidden" name="id_a" value="<?php echo $elem['id']; ?>"><input type="submit" value="supprimer"></form></td></tr>
                                <?php
                                $cpt=$cpt+1;     }
                                ?>
                            </table>
                 </div>

                 <div class="membre_admin">

                            <h1>suprimer cour</h1>
                            <table class="table" >
                                <tr class="thead"><th>numero</th><th>nom</th><th>supression</th></tr>
                                <?php
                                    $cpt=1;
                                        while ($elem=$reqC->fetch()) {
                                            
                                ?>
                                            <tr height="40px" ><td width="8%"><?php echo $cpt;  ?></td><td width="17%"><?php echo $elem['nom'];  ?></td><td width="15%"><form action="traitement_supresion_cour.php" method="post"> <input type="hidden" name="id_b" value="<?php echo $elem['id']; ?>"><input type="submit" value="supprimer"></form></td></tr>
                                <?php
                                $cpt=$cpt+1;     }
                                ?>
                            </table>
                 </div>

                 <div class="membre_admin">

                            <h1>suprimer epreuve</h1>
                            <table class="table" >
                                <tr class="thead"><th>numero</th><th>nom</th><th>supression</th></tr>
                                <?php
                                    $cpt=1;
                                        while ($elem=$reqE->fetch()) {
                                            
                                ?>
                                            <tr height="40px" ><td width="8%"><?php echo $cpt;  ?></td><td width="17%"><?php echo $elem['nom'];  ?></td><td width="15%"><form action="traitement_supresion_epreuve.php" method="post"> <input type="hidden" name="id_c" value="<?php echo $elem['id']; ?>"><input type="submit" value="supprimer"></form></td></tr>
                                <?php
                                $cpt=$cpt+1;     }
                                ?>
                            </table>
                 </div>

     </article>
<?php 
        include("bas.php");
?>
</body>
</html>