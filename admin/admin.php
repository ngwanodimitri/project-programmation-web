<?php
    session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="admin.css">
</head>
<body>
   <?php 
        include("haut.php");
   ?>
        <article class="droite">
            <div class="div1"> 
                <a href="/project-programmation-web/admin/profil.php" class="champ1"> <div >  <h1 class="h1">profil</h1> <br> Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit, modi! Deserunt culpa modi provident. Tempora rem quos illo error saepe atque, illum omnis quasi porro deserunt nihil similique adipisci. Reprehenderit? </div></a>
                <a href="/project-programmation-web/admin/membre.php" class="champ1"> <div > <h1 class="h1">membres</h1><br> Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit, modi! Deserunt culpa modi provident. Tempora rem quos illo error saepe atque, illum omnis quasi porro deserunt nihil similique adipisci. Reprehenderit?</div></a>
            </div>
            <div class="div2">
                <a href="/project-programmation-web/admin/ajout.php" class="champ2"> <div >  <h1 class="h1"> Ajout </h1> <br> Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit, modi! Deserunt culpa modi provident. Tempora rem quos illo error saepe atque, illum omnis quasi porro deserunt nihil similique adipisci. Reprehenderit?</div></a>
                <a href="/project-programmation-web/admin/supression.php" class="champ2"><div > <h1 class="h1">Suppression</h1> <br> Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit, modi! Deserunt culpa modi provident. Tempora rem quos illo error saepe atque, illum omnis quasi porro deserunt nihil similique adipisci. Reprehenderit?</div></a>
            </div>
            
            
        </article>
    <?php 
        include("bas.php");
   ?>
</body>
</html>